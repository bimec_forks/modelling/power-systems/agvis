.. _development:

===========
Development
===========

This section contains a more in-depth look at AGVis’s structure and implementation for those interested in possibly contributing to AGVis. Think of this as something of an abridged glossary: the following pages will have detailed descriptions of important variables and methods along with some commentary on AGVis’s structure. Some variables that are either self-contained to a page or self-explanatory will not be listed. Please note that AGVis was developed by two different people (with a third currently being trained) with little overlap with regard to when they were working on it. This means that some of the commentary and discussion for certain sections are going to be based on assumptions and interpretation. 

.. toctree::
    :maxdepth: 2

    basics
    ind
    window
    canvas
    zone
    topology
    contour
    search
    communication
    timebox
    playback
    config
    layer
    topmulti
    contmulti
    playmulti
